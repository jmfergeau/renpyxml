﻿# You can place the script of your game in this file.

# Declare images below this line, using the image statement.
# eg. image eileen happy = "eileen_happy.png"

# Declare characters used by this game.
define e = Character('Eileen', color="#c8ffc8")

# Define xml file.
define xmlfile = 'data.xml'

# Put here the number of questions your script has. Don't worry for the script counting by 0,
# we'll substract 1 automatically later
define totalquestions = 3

# Other values used by the script goes here.
define questiontemp = None
define thequestion = None
define rightanswer = None
define answer1 = None
define answer2 = None
define answer3 = None
define answer4 = None
define answer1id = None
define answer2id = None
define answer3id = None
define answer4id = None
define answerlist = None

label splashscreen:
    
    python:
        # import xml related libraries
        import xml.etree.ElementTree as ET
        
        # parse the data.xml file
        qlist = ET.parse(renpy.file(xmlfile))
        root = qlist.getroot()
        
        answerlist = qlist.getElementsByTagName('answer')
    
    return

# The game starts here.
label start:
    
    # This gets the root tag used in the xml. In this example, it is "renpy". Check on data.xml. 
    $ questiontemp = root.tag
    "Root tag name: %(questiontemp)s"
    
    # This gets the text in the second tag (the "q") in the first "question" block.
    $ questiontemp = root[0][1].text
    "child tag content: %(questiontemp)s"
    
label quiz:
    "Now, choosing a random question."
    
    # In here, the game will choose a question randomly.
    # Note that we substract 1 to totalquestions because the scripts starts counting by 0
    $ questiontemp = renpy.random.randint(0,(totalquestions-1))
    
    # Fetch the right answer here
    $ rightanswer = root[questiontemp][2].text
    
    # Setting the variables for the answers and question
    $ thequestion = root[questiontemp][1].text
    $ answer1 = root[questiontemp][3].text
    $ answer2 = root[questiontemp][4].text
    $ answer3 = root[questiontemp][5].text
    $ answer4 = root[questiontemp][6].text
    
    $ answer1id = qlist.find('./question[@id='questiontemp']/answer').attrib['id']
    $ answer2id = answerlist[questiontemp][4].attributes['answer'].value
    $ answer3id = answerlist[questiontemp][5].attributes['answer'].value
    $ answer4id = answerlist[questiontemp][6].attributes['answer'].value
    
    # Menu of the question
    "%(thequestion)s"
    
    menu:
        "%(answer1)s":
            # checks if the answer is correct by comparing the answer id and the rightanswer.
            # If anyone knows how to make this as a custom function, you're welcome to teach me ! :p
            "you choose %(answer1id)s"
            if answer1id == rightanswer:
                "Good answer ! :)"
                jump quiz
            else:
                "Wrong answer ! :("
                jump quiz
            
        "%(answer2)s":
            if root[questiontemp][4].attrib == rightanswer:
                "Good answer ! :)"
                jump quiz
            else:
                "Wrong answer ! :("
                jump quiz
            
        "%(answer3)s":
            if root[questiontemp][5].attrib == rightanswer:
                "Good answer ! :)"
                jump quiz
            else:
                "Wrong answer ! :("
                jump quiz
            
        "%(answer4)s":
            if root[questiontemp][6].attrib == rightanswer:
                "Good answer ! :)"
                jump quiz
            else:
                "Wrong answer ! :("
                jump quiz
            

    return
